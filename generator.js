function isValidHttpUrl(string) {
    let url;
    try {
      url = new URL(string);
    } catch (_) {
      return false;
    }
    return url.protocol === "http:" || url.protocol === "https:";
}

var iframe
var iframeDocument

function init(){
    iframe = document.getElementById('preview');
    iframeDocument = iframe.contentDocument || iframe.contentWindow.document;
}

function addBasicInfo(){
    let reader = new FileReader();
    SiteTitle = document.getElementById("Title").value
    try {
        ProfilePicture = document.getElementById("ProfilePictureUpload").files[0]
        reader.readAsDataURL(ProfilePicture)
        reader.onload = function() {
            iframeDocument.getElementById("profilepicture").src=reader.result;
          };
    } catch (error) {
        console.log("No Image Selected")
    }
    iframeDocument.getElementById("Title").textContent = SiteTitle
    iframeDocument.getElementById("title").textContent = SiteTitle
}

function add(){
    //alert("working!")
    AltString = document.getElementById("AltString").value
    Link = document.getElementById("Link").value
    ServiceName = document.getElementById("ServiceName").value
    
    document.getElementById("AltString").value = ""
    document.getElementById("Link").value = ""
    document.getElementById("ServiceName").value = ""

    addBasicInfo()
    var table = iframeDocument.getElementById("table")
    var row = table.insertRow(0);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);

    cell1.innerHTML = ServiceName+":";
    if (isValidHttpUrl(Link)){
        if(AltString == null || AltString.trim() == ""){
            cell2.innerHTML = "<a target='_blank' href='"+Link+"'>"+Link+"</a>";
        }else{
            cell2.innerHTML = "<a target='_blank' href='"+Link+"'>"+AltString+"</a>";
        }
    }else{
        cell2.innerHTML = Link; 
    }
}

function closeOverlay(){
    document.getElementById("Overlay").style.width = "0%";
}

async function generate(){
    addBasicInfo()
    document.getElementById("Overlay").style.width = "100%";
    await new Promise(resolve => setTimeout(resolve, 2000));
    document.getElementById("Overlay").style.width = "0%";
    var zip = new JSZip();
    zip.file("index.html",iframeDocument.documentElement.innerHTML)
    console.log(iframeDocument.documentElement.innerHTML)
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET", "boilerplate/base.css", false);
    xmlhttp.send();
    if (xmlhttp.status==200) {
      result = xmlhttp.responseText;
    }
    zip.file("base.css",result)
    
    zip.generateAsync({type : "blob"})
        .then(function (blob) {
            saveAs(blob, "Site.zip");
    });
}